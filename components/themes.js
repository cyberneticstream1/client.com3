import {createTheme} from "@mui/material/styles";

export const whiteTextBlackPaper = createTheme({
    palette: {

        primary: {
            main: "#FFFFFF"
        },
        secondary: {
            main: "#FFFFFF"
        },
        text: {
            primary: "#FFFFFF",
            secondary: "#FFFFFF"
        },
        background: {
            paper: "#000000",
            default: "#000000",
        },
    },
    typography: {
        fontFamily: "var(--font-inter)",
    }
});


