"use client";
import React from "react";
import {ThemeContext} from "./ThemeContext"

export default function PrimaryText({childrenMenu, childrenBody}){
    const [ua] = React.useState(React.useContext(ThemeContext))
    const [mobileVisibility, setMobileVisibility] = React.useState("hidden")
    const [pCVisibility, setPCVisibility] = React.useState("hidden")

    React.useEffect(() => {
        if (ua.isMobile) {
            setMobileVisibility("block")
            setPCVisibility("hidden")
        } else{
            setMobileVisibility("hidden")
            setPCVisibility("block")
        }
    },[])

    return(
            <>
                <div className={mobileVisibility}>
                    <div className = {"fixed bottom-0 w-11/12 h-80 left-1/2 -translate-x-1/2 rounded-xl z-10"}>
                        <div  className={"text-white text-sans text-4xl leading-tight text-center leading-relaxed"}>
                        {childrenMenu}
                        </div>
                    {childrenBody}
                    </div>
                </div>


            <div className={pCVisibility}>
                <div className = {"fixed top-1/2 -translate-y-1/2 right-1/4 translate-x-1/2  w-96 h-96 z-10"}>
                    <div  className={"text-white text-sans text-4xl leading-tight text-center leading-relaxed"}>
                        {childrenMenu}
                    </div>
                    {childrenBody}
                </div>
            </div>

            </>
    )
}